<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserController extends Controller
{
    /**
   * @Route("/user", name="user_info")
   */
  public function infoAction(Request $request)
  {
    $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
      return $this->render('user/index.html.twig', array('users' => $users));
  }
    /**
   * @Route("/user/create", name="user_create")
   */
  public function createAction(Request $request)
  {
    $user = new User;
    $form = $this->createFormBuilder($user)
            ->add('fname', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('lname', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('username', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('workadd', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('postalcode', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Create User', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $fname = $form['fname']->getData();
      $lname = $form['lname']->getData();
      $username = $form['username']->getData();
      $email = $form['email']->getData();
      $workadd = $form['workadd']->getData();
      $postalcode = $form['postalcode']->getData();

    $user->setFname($fname);
    $user->setLname($lname);
    $user->setUsername($username);
    $user->setEmail($email);
    $user->setWorkadd($workadd);
    $user->setPostalcode($postalcode);

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    $this->addFlash('notice', 'User added!');
    return $this->redirectToRoute('user_info');

  }
      return $this->render('user/create.html.twig', array('form' => $form->createView()));
  }
    /**
   * @Route("/user/edit/{id}", name="user_edit")
   */
  public function editAction($id, Request $request)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

    $user->setFname($user->getFname());
    $user->setLname($user->getLname());
    $user->setUsername($user->getUsername());
    $user->setEmail($user->getEmail());
    $user->setWorkadd($user->getWorkadd());
    $user->setPostalcode($user->getPostalcode());

    $form = $this->createFormBuilder($user)
            ->add('fname', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('lname', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('username', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('workadd', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('postalcode', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Update User', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $fname = $form['fname']->getData();
      $lname = $form['lname']->getData();
      $username = $form['username']->getData();
      $email = $form['email']->getData();
      $workadd = $form['workadd']->getData();
      $postalcode = $form['postalcode']->getData();

    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('AppBundle:User')->find($id);

    $user->setFname($fname);
    $user->setLname($lname);
    $user->setUsername($username);
    $user->setEmail($email);
    $user->setWorkadd($workadd);
    $user->setPostalcode($postalcode);

    $em->flush();

    $this->addFlash('notice', 'User Updated!');
    return $this->redirectToRoute('user_info');

  }

    return $this->render('user/edit.html.twig', array(
      'user' => $user,
      'form'=> $form->createView()
  ));
  }
  /**
  * @Route("/user/details/{id}", name="user_details")
  */
  public function detailsAction($id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
    return $this->render('user/details.html.twig', array(
      'user' => $user
    ));
  }
  /**
  * @Route("/user/delete/{id}", name="user_delete")
  */
  public function deleteAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('AppBundle:User')->find($id);
    $em->remove($user);
    $em->flush();
    $this->addFlash('notice', 'User Removed!');
    return $this->redirectToRoute('user_info');
  }
}
